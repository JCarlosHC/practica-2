Feature: Lista Productos
    Scenario: Cargar lista de productos
        When we request products list
        Then we should receive
            | nombre | descripcion |
            | Movil XL | Un teléfono grande con una de las mejores pantallas |
            | Movil Mini | Un teléfono mediano con una de las mejores camaras |
            | Movil Standard |              |
